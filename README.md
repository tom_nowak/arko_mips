Histogram of 8-bit BGR .bmp image using MIPS assembly (MARS simulator).
Note that histogram template filename is hard-coded: "template.bmp" in line 36, so program must be run from directory containing template.bmp.
