##################################################################################################################
# ARKO projekt MIPS - wyznaczanie histogramu obrazka bmp
# Tomasz Nowak, nr albumu 277177
# Nazwy etykiet i komentarze w języku angielskim ,zgodnie z powszechnie przyjętym zwyczajem w świecie IT.
##################################################################################################################


# I am aware of MIPS calling convention, but I do not have to comply with it. 
# In my own convention, I use registers s, for "global" variables 
# and t for temporary variables (in one code segment, segments are separated by big comments).


##################################################################################################################
# START general declarations
##################################################################################################################
	
# Basic bmp header is 14 bytes long. Useful data in .bmp header is at addresses 2 and 10. In order to read
# data properly in words (aligned 4), I allocate buffer of size 16 (I will write starting at address header+2).
	.data
	
	.align 4
header:	.space 16
# Space for long part of header and pixel data will be allocated dynamically.
filename:
	.space 128	# filename to read from, later - will be used to create histogram filename

# 3 hash tables for 256 4-byte numbers - histograms of colours (red, green, blue)
	.align 4
histR:	.space 1024
	.align 4
histG:	.space 1024
	.align 4
histB:	.space 1024

graph_template_filename:
	.asciiz	"template.bmp"
			   
	.eqv HISTOGRAM_SIZE 1279994

	.macro PRINT_STR (%str)
	.data
myLabel: .asciiz %str
	.text
	li	$v0, 4
	la	$a0, myLabel
	syscall
	.end_macro
	
	.macro EXIT
	li	$v0, 10
	syscall
	.end_macro

##################################################################################################################
# END of general declarations
##################################################################################################################	


##################################################################################################################
# START initialization

# Read input filename from user, open file, save it in memory, read file header. 
# After this block of instructions data will be saved in registers
# $s0 - address of allocated memory
# $s1 - size of allocated memory
# $s2 - addres of image data begin 
# $s3 - address of image data end
# $s4 - number of bytes from row begin to the end of the last 12-byte block in each row
# $s5 - number of bytes from row begin to the end of the last not padded 4-byte word
# $s6 - number of bytes with image data in the last block (number of padding bytes: 4-$s6)
# $s7 - size of histogram image
##################################################################################################################

	.text	
read_filename:
	PRINT_STR("Path to input file:\n")
	li	$v0, 8		# read string
	la	$a0, filename
	li	$a1, 112	# less than 128, because this buffer will be used later for histogram's longer filename
	syscall

	# After reading filename, the string ends with '\n\0' ('\0' only if 95 chars were written).
	# It makes it impossible to open file with this name - \n has to be removed:
	li	$t9, '\n'
	move	$t2, $a0	
remove_endline_from_filename:
	lb	$t1, ($t2)
	addi	$t2, $t2, 1
	bgt	$t1, $t9, remove_endline_from_filename
	sb	$zero, -1($t2)

open_header_file:
	li	$v0, 13		# open file	
	li	$a1, 0
	li	$a2, 0
	syscall			# now there should be file descriptor in $v0
	move	$t0, $v0	# save file descriptor
	bgt	$t0, $zero, create_histogram_filename
	# if there was no jump - error opening file
	PRINT_STR("Error opening file.\n")
	EXIT
	
error_reading_file:
	move	$a0, $t0	# file descriptor saved in open_header_file (this label might be jumped to from code below)
	li	$v0, 16		# close file
	syscall
	PRINT_STR("Error reading file.\n")
	# MARS makes it impossible to free memory - look comment after initialization
	EXIT

unsupported_file_format_BM:
	move	$a0, $t0	# file descriptor saved in open_header_file (this label might be jumped to from code below)
	li	$v0, 16		# close file
	syscall
	PRINT_STR("Unsupported file format (expected .bmp file with proper header).\n")
	EXIT
	
unsupported_file_format_BGR:
	move	$a0, $t0	# file descriptor saved in open_header_file (this label might be jumped to from code below)
	li	$v0, 16		# close file
	syscall
	PRINT_STR("Unsupported file format (expected 24-bit BGR pixels).\n")
	# MARS makes it impossible to free memory - look comment after initialization
	EXIT
	
create_histogram_filename:
	# add "_histogram" to filename, .bmp \0 at the end
	.macro ADD_CHAR(%x)
	li	$t1, %x
	sb	$t1, ($t2)
	addi	$t2, $t2, 1
	.end_macro
	subiu	$t2, $t2, 5	# after read_filename $t2 is after the end of filename string, change it to '.' before 'bmp'
	ADD_CHAR('_')
	ADD_CHAR('h')
	ADD_CHAR('i')
	ADD_CHAR('s')
	ADD_CHAR('t')
	ADD_CHAR('o')
	ADD_CHAR('g')
	ADD_CHAR('r')
	ADD_CHAR('a')
	ADD_CHAR('m')
	ADD_CHAR('.')
	ADD_CHAR('b')
	ADD_CHAR('m')
	ADD_CHAR('p')
	ADD_CHAR(0)
	
read_header:
	PRINT_STR("Reading input file...\n")
	li	$v0, 14		# read	from file
	move	$a0, $t0	# $a0 = file descriptor (saved in open_header_file)
	la	$a1, header
	addi	$a1, $a1, 2	# start at address +2, so that useful data form header will be aligned 4
	li	$a2, 14		# read 14 bytes - the whole basic file header
	syscall
	bne	$v0, $a2, error_reading_file
	lh	$t2, ($a1)
	li	$t3, 0x4d42	# reversed "BM" (reversed because first letter should be at less significant byte)
	bne	$t2, $t3, unsupported_file_format_BM

get_file_size:
	lw	$s1, 2($a1)	# load file size from basic file header ($a1 - pointer to header's begin, saved in read_header)
	subiu	$s1, $s1, 14	# subtract 14 to get remaining file size (14 bytes have been already read)
	lw	$t2, 10($a1)	# load offset to know where image data starts
	subiu	$t2, $t2, 14	# subtract 14 to get distance between begin of unread (yet) part of header and image pixel data
	move	$t3, $s1	# copy remaining file size because $s1 might be changed (see below)
	# Allocate greater value out of: HISTOGRAM_SIZE, (input image size - 14 read bytes) - memory will be reused later
	li	$s7, HISTOGRAM_SIZE
	blt	$s7, $s1, allocate_memory
	move	$s1, $s7	# allocate HISTOGRAM_SIZE instead of input image size left to read
	
allocate_memory:
	li	$v0, 9		# sbrk
	move	$a0, $s1	# remaining file size, loaded in get_file_size
	syscall
	move	$s0, $v0	# save address of allocated memory

read_remaining_data:
	li	$v0, 14		# read from file
	move	$a0, $t0	# $a0 = file descriptor (saved in open_header_file)
	move	$a1, $s0	# address of dynamically allocated memory
	move	$a2, $t3	# number of bytes left to read (saved in get_fize_size)
	syscall
	bne	$v0, $t3, error_reading_file

check_bits_per_pixel:
	li 	$t4, 24
	lh	$t5, 14($s0)
	bne	$t5, $t4, unsupported_file_format_BGR
	
close_file:
	li	$v0, 16		# close file
	# $a0 = $t0 since line 87 - no need to change it
	syscall

save_image_info:
	addu	$s2, $s0, $t2	# image data address = begin of allocated memory + offset (from get_file_size)
	lw	$s3, 20($s0)	# image size in bytes, will be changed below
	addu	$s3, $s3, $s2	# address of image data end
	lw	$t3, 4($s0)	# image width, temporary (will be used below)

	# Reading image row will be divided into 3 parts:
	# reading in blocks: 3 words (12 bytes - 4 pixels). In each word colours in order: BGRB,GRBG,RBGR
	# reading 0, 1 or 2 reamaining whole words (whole - all word bytes belong to image data, not padding)
	# reading 0, 1, 2 or 3 bytes from the last word
	# Values in $s4, $s5 and $s6 will be used in these steps of iterating over one table row.
	
	li	$t9, 0xfffffffc	# bit mask to perform operation: x -= x%4	
	and	$s4, $t3, $t9	# width-width%4: number of pixels from row begin to the end of the last 12-byte block
	sll	$t4, $s4, 1	# multiply $s4 by 2 
	addu	$s4, $s4, $t4	# $s4=3*$s4 - number of bytes from row begin to the end of the last 12-byte block
	sll	$t4, $t3, 1	# multiply image width by 2 (used in line below)
	addu	$s6, $t4, $t3	# $s6 = 2*$t3 + $t3 = 3*(image width) = (length of one row in bytes, without padding)
	and	$s5, $s6, $t9	# number of bytes from row begin to the end of the last not padded 4-byte word
	subu	$s6, $s6, $s5	# number of bytes with image data in the last block (number of padding bytes: 4-$s6)
	
#show_testing_info:		# deleted in release version
#	PRINT_STR("\long part of header size: ")
#	li	$v0, 1		# print int
#	lw	$a0, ($s0)	# size of long part of header
#	syscall

#	PRINT_STR("\nwidth: ")
#	li	$v0, 1		# print int
#	lw	$a0, 4($s0)	# width
#	syscall

#	PRINT_STR("\nheight: ")
#	li	$v0, 1		# print int
#	lw	$a0, 8($s0)	# height
#	syscall

#	PRINT_STR("\nimage data size: ")
#	li	$v0, 1		# print int
#	lw	$a0, 20($s0)	# image data size
#	syscall
#	PRINT_STR("\n\n")

##################################################################################################################
# END of initialization

# Important image properties saved in registers:
# $s0 - address of allocated memory
# $s1 - size of allocated memory
# $s2 - addres of image data begin 
# $s3 - address of image data end
# $s4 - number of bytes from row begin to the end of the last 12-byte block in each row
# $s5 - number of bytes from row begin to the end of the last not padded 4-byte word
# $s6 - number of bytes with image data in the last block (number of padding bytes: 4-$s6)
# $s7 - size of histogram image

# Note - the vaule stored in $s1 is meant to enable memory deallocation, but the
# following code causes runtime error (MARS does not allow sbrk to have a negative argument):
#deallocate_memory:
#	li	$v0, 9		# sbrk
	# Set value of $a0 to (-$s1) to decrease number of bytes allocated by the number previously allocated
#	not	$a0, $s1	# in U2 sign can be changed by negation ...
#	addi	$a0, $a0, 1	# and adding one
#	syscall	
##################################################################################################################


##################################################################################################################
# START calculating histogram

# For each image pixel increment the corresponding value in histogram hash table.

# After this block of instructions data will be saved in registers:
# $s0 - address of allocated memory
# $s1 - size of allocated memory
# $s2 - histogram for blue channel
# $s3 - histogram for green channel
# $s4 - histogram for red channel
# $s7 - size of histogram image
##################################################################################################################

calculate_histogram:
	PRINT_STR("Calculating histogram...\n")

	# Registers used as constants:
	la	$a0, histB	# hash table for histogram blue
	la	$a1, histG	# hash table for histogram green
	la	$a2, histR	# hash table for histogram red
	li	$t6, 0xff000000	# bit mask to extract byte out of word
	li	$t7, 0x00ff0000	# bit mask to extract byte out of word
	# $s3 - address of image data end
	# $s4 - number of bytes from row begin to the end of the last 12-byte block in each row
	# $s5 - number of bytes from row begin to the end of the last not padded 4-byte word
	# $s6 - number of bytes with image data in the last block (number of padding bytes: 4-$s6)
	
	# Registers used as variables:
	# $s2 - (initialized before as image data begin) used to iterate over image data
	# $t0 - address after the last long 12-byte block
	# $t1 - address after the last 4-byte non-padded block
	# $t2 - used to iterate in the last (padded) word
	# $t3 - word loaded from image (at address $s2)
	# $t4 - pixel colour value (one byte extracted from $t3), then - address in histogram hash table
	# $t5 - to temporarily load value from histogram, increment it, and store it
	
	# Macros to simplify main loop below
	.macro LOAD_WORD
	lw	$t3, ($s2)
	addu	$s2, $s2, 4	# go to the next word
	.end_macro

	# In GET_BYTE_* array index is changed into offset - number of bytes (for _4, _3 and _2 this allows to save one
	# instruction - move once instead of shifting right to position of byte 1, and then shifting left 2 to multiply).
	.macro GET_BYTE_4
	and	$t4, $t3, $t6	# extract byte
	srl	$t4, $t4, 22	# move byte to least significant position in word and instantly multiply by 4
	.end_macro
	
	.macro GET_BYTE_3
	and	$t4, $t3, $t7
	srl	$t4, $t4, 14	# move byte to least significant position in word and instantly multiply by 4
	.end_macro
	
	.macro GET_BYTE_2
	andi	$t4, $t3, 0xff00
	srl	$t4, $t4, 6	# move byte to least significant position in word and instantly multiply by 4
	.end_macro
	
	.macro GET_BYTE_1
	andi	$t4, $t3, 0x00ff
	sll	$t4, $t4, 2	# multiply by 4
	.end_macro
	
	.macro ADD_TO_HISTOGRAM(%x) 
	# %x should be $a0 for blue, $a1 for green, $a2 for red
	# array index has been changed into offset - number of bytes (multiplied by 4 in GET_BYTE)
	addu	$t4, $t4, %x	# add address of hash table to get address of particular element
	lw	$t5, ($t4)
	addi	$t5, $t5, 1	# increment value in the hash table
	sw	$t5, ($t4)
	.end_macro

	beq	$s2, $s3, end_calculating_histogram
for_each_row:
	addu	$t0, $s2, $s4	# address after the last long 12-byte block
	addu	$t1, $s2, $s5	# address after the last 4-byte non-padded block
	# for very small images thee might be no 12-byte blocks, so loop condition is checked at the beginning
	beq	$s2, $t0, for_end_of_row
	
for_each_12_bytes:
	# loop consists of so many operations (read 3 words with colours: BGRB,GRBG,RBGR), because
	# the sequence BGR appears at the beginning of the 4-byte word after reading 12 bytes (3 words / 4 pixels)
	LOAD_WORD
	GET_BYTE_1
	ADD_TO_HISTOGRAM($a0)
	GET_BYTE_2
	ADD_TO_HISTOGRAM($a1)
	GET_BYTE_3
	ADD_TO_HISTOGRAM($a2)
	GET_BYTE_4
	ADD_TO_HISTOGRAM($a0)
	LOAD_WORD
	GET_BYTE_1
	ADD_TO_HISTOGRAM($a1)
	GET_BYTE_2
	ADD_TO_HISTOGRAM($a2)
	GET_BYTE_3
	ADD_TO_HISTOGRAM($a0)
	GET_BYTE_4
	ADD_TO_HISTOGRAM($a1)
	LOAD_WORD
	GET_BYTE_1
	ADD_TO_HISTOGRAM($a2)
	GET_BYTE_2
	ADD_TO_HISTOGRAM($a0)
	GET_BYTE_3
	ADD_TO_HISTOGRAM($a1)
	GET_BYTE_4
	ADD_TO_HISTOGRAM($a2)	
	bne	$s2, $t0, for_each_12_bytes

for_end_of_row:
	bne	$s2, $t1, for_remaining_words
	beq	$s2, $s3, end_calculating_histogram
	beqz	$s6, for_each_row

# If there are no remaining words, read 3 remaining bytes, starting with byte for histogram $a0, ending with $a2.
# In each pixel there must be a representation for each colour, so the last histogram saved should always be $a2.	
for_remaining_bytes_0:	# 0 - the first byte should be added to histogram ($a0), similar labels: *_1, *_2 - below for_remaining_words
	# The word which is going to be loaded might beloneg to another row - in this case jump.
	LOAD_WORD
	GET_BYTE_1
	ADD_TO_HISTOGRAM($a0)
	GET_BYTE_2
	ADD_TO_HISTOGRAM($a1)
	GET_BYTE_3
	ADD_TO_HISTOGRAM($a2)
	beq	$s2, $s3, end_calculating_histogram
	j	for_each_row
		
for_remaining_words:
	LOAD_WORD
	GET_BYTE_1
	ADD_TO_HISTOGRAM($a0)
	GET_BYTE_2
	ADD_TO_HISTOGRAM($a1)
	GET_BYTE_3
	ADD_TO_HISTOGRAM($a2)
	GET_BYTE_4
	ADD_TO_HISTOGRAM($a0)
	beq	$s2, $t1, for_remaining_bytes_1
	LOAD_WORD
	GET_BYTE_1
	ADD_TO_HISTOGRAM($a1)
	GET_BYTE_2
	ADD_TO_HISTOGRAM($a2)
	GET_BYTE_3
	ADD_TO_HISTOGRAM($a0)
	GET_BYTE_4
	ADD_TO_HISTOGRAM($a1)
	# the next word must have padded bytes, otherwise it would have been included in for_each_12_bytes

for_remaining_bytes_2:	# 2 - the first byte should be added to histogram ($a2)
	# Do not check if $s6==0, because the last histogram saved should always be $a2.
	LOAD_WORD
	GET_BYTE_1
	ADD_TO_HISTOGRAM($a2)
	bne	$s2, $s3, for_each_row
	j	end_calculating_histogram

for_remaining_bytes_1:	# 1 - the first byte should be added to histogram ($a1)
	# Do not check if $s6==0, because the last histogram saved should always be $a2.
	LOAD_WORD
	GET_BYTE_1
	ADD_TO_HISTOGRAM($a1)
	GET_BYTE_2
	ADD_TO_HISTOGRAM($a2)
	bne	$s2, $s3, for_each_row
	
end_calculating_histogram:
	# According to my calling convention, I save values which will be used in the next block.
	move	$s2, $a0
	move	$s3, $a1
	move	$s4, $a2
	
			
##################################################################################################################
# END of calculating histogram

# Useful data is in registers:
# $s0 - address of allocated memory
# $s1 - size of allocated memory
# $s2 - histogram for blue channel
# $s3 - histogram for green channel
# $s4 - histogram for red channel
# $s7 - size of histogram image
##################################################################################################################


##################################################################################################################
# START preparing to draw histogram

# Find the gratest colour value in all histograms and read histogram graph template to memory.

# After this block of instructions data will be saved in registers
# $s0 - address of allocated memory
# $s1 - size of allocated memory
# $s2 - histogram for blue channel
# $s3 - histogram for green channel
# $s4 - histogram for red channel
# $s5 - max value in all histograms
# $s6 - last word loaded (after loop - from histogram red) - to avoid loading it once again
# $s7 - size of histogram image
##################################################################################################################


read_histogram_template:
	PRINT_STR("Reading histogram template...\n")
	la	$a0, graph_template_filename
	li	$v0, 13		# open file	
	li	$a1, 0
	li	$a2, 0
	syscall			# now there should be file descriptor in $v0
	ble	$v0, $zero, error_opening_histogram_template
	move	$a0, $v0	# $a0 = file descriptor
	li	$v0, 14		# read from file
	move	$a1, $s0	# address of dynamically allocated memory (input image is no longer needed, it will be overwritten)
	move	$a2, $s7	# number of bytes to read - histogram template size
	syscall
	beq	$v0, $s7, find_max

error_reading_histogram_template:
	li	$v0, 16		# close file
	syscall
	PRINT_STR("Error reading histogram template.\n")
	# MARS makes it impossible to free memory - look comment after initialization
	EXIT
	
error_opening_histogram_template:
	PRINT_STR("Error opening histogram template.\n")
	# MARS makes it impossible to free memory - look comment after initialization
	EXIT

find_max:
	li	$v0, 16		# close file
	syscall
	PRINT_STR("Drawing histogram...\n")
	# Register used:
	move	$t0, $s2	# iterator for histogram blue
	move	$t1, $s3	# iterator for histogram green
	move	$t2, $s4	# iterator for histogram red, not $t2, because it will be used in the next block
	addiu	$t3, $t2, 1024	# end of histogram red
	# $s6 - last word loaded
	move	$s5, $zero 	# max value found so far
find_max_loop:
	lw	$s6, ($t0)
	addi	$t0, $t0, 4
	blt	$s6, $s5, not_max1
	move	$s5, $s6
not_max1:
	lw	$s6, ($t1)
	addi	$t1, $t1, 4
	blt	$s6, $s5, not_max2
	move	$s5, $s6
not_max2:
	lw	$s6, ($t2)
	addi	$t2, $t2, 4
	blt	$s6, $s5, not_max3
	move	$s5, $s6
not_max3:
	# iterating for all histograms at once, so only one iterator needs to be checked:
	bne	$t2, $t3, find_max_loop
	
##################################################################################################################
# END of preparing to draw histogram

# Useful data in registers:
# $s0 - address of allocated memory
# $s1 - size of allocated memory
# $s2 - histogram for blue channel
# $s3 - histogram for green channel
# $s4 - histogram for red channel
# $s5 - max value in all histograms
# $s6 - last word loaded (after loop - from histogram red) - to avoid loading it once again
# $s7 - size of histogram image
##################################################################################################################	


##################################################################################################################
# START drawing histogram

# For each histogram element normalize it, so that all values are betwenn 0 and 512 and then draw column of 
# appropriate colour (its graphical representation) on the histogram template. All number constants are connected
# with template properties: colours 24 bit BGR; offset (image data starts at address): 122; image width: 808,
# height: 528; each of the three plots (for B,G,R) can be up to 512 pixels high and is exactly 256 pixels wide. 
# Points (0,0) of histograms are at positions: B(8,7), G(276, 7), R(544, 7). Due to the way of coding
# pixels (all addresses in a row next to one another), offset of pixel (x,y) can be calculated using the foromula: 
# offset(x,y)=122+3*(808*y+x).
# Add 0 for byte with blue, 1 for green 2 for red. Using this formula, I precalculated offsets of beginnings 
# of histograms (including colour address): B:17114, G:17919, R:18724. Ends of histograms are (256-1)*3=765
# bytes further: B:17879, G:18684, R:19489. Values confirmed empirically using hex editor.
##################################################################################################################

draw_histograms:
	# Registers used:
	# $s0 - address of allocated memory
	# $s2 - histogram for blue channel
	# $s3 - histogram for green channel
	# $s4 - histogram for red channel
	# $s5 - max value in all histograms
	# $s6 - last word loaded (starting at end of histogram red)
	# $t0 - the lowest point of the column being drawn
	# $t1 - iterator with image byte address while drawing a column
	# $t2 - iterator for histograms
	li	$t8, 2424	# offset between two image points with y difference 1 and the same x
	li	$t9, 255	# value of colour which will be stored under $t0 addres
	
	.macro	DRAW
	subiu	$t2, $t2, 4	# go to another value in histogram (iterating backwards)
	lw	$s6, ($t2)	# read value from histogram
	jal	draw_column	# call function (declared at the bottom of this block of instructions)
	subiu	$t0, $t0, 3	# go to another column	(iterating backwards)
	.end_macro
	
draw_red_histogram:
	addiu	$t0, $s0, 19489	# starting at end of red histogram graph (address - see long comment before this block)
	addiu	$t2, $s4, 1020	# one before the last element in histogram red table
	# there is no lw - value has been loaded while looking for max element
	jal	draw_column
	subiu	$t0, $t0, 3	# go to another column	(iterating backwards)
draw_red_histogram_loop:
	DRAW
	bne	$t2, $s4, draw_red_histogram_loop
draw_green_histogram:
	addiu	$t0, $s0, 18684	# end of green histogram graph (address - see long comment before this block)
	addiu	$t2, $s3, 1024	# the last element in histogram green table
draw_green_histogram_loop:
	DRAW
	bne	$t2, $s3, draw_green_histogram_loop
draw_blue_histogram:
	addiu	$t0, $s0, 17879	# end of blue histogram graph (address - see long comment before this block)
	addiu	$t2, $s2, 1024	# the last element in histogram blue table
draw_blue_histogram_loop:
	DRAW
	bne	$t2, $s2, draw_blue_histogram_loop

save_histogram_to_file:
	PRINT_STR("Saving histogram to file...\n")
	li	$v0, 13		# open file
	la	$a0, filename	# filename has already been changed in create_histogram_filename
	li	$a1, 1		# flag for writing
	li	$a2, 0		# mode is ignored
	syscall			# file descriptor returned in $v0
	ble	$v0, $zero, error_opening_histogram
	move	$a0, $v0	# $a0 = file descriptor
	move	$a1, $s0	# address of histogram image
	move	$a2, $s7	# size of histogram image
	li	$v0, 15		# write to file
	syscall
	bne	$v0, $s7, error_saving_histogram
	li	$v0, 16		# close file
	syscall
	PRINT_STR("Histogram saved succesfully in file:\n")
	li	$v0, 4
	la	$a0, filename
	syscall
	# MARS makes it impossible to free memory - look comment after initialization
	EXIT
	
draw_column:
	# Multiply by 512, because pixels in y axis of histogram have range from 0 to 512. I am aware of the risk of
	# unsigned number overflow, but it is not very likely that a pixel value will be as great as 2^23 (8388608).
	# Even if there was a big single-coloured image, MARS has too little memory for images of such size!
	# In general situation (not simplified and optimized like here) there would be any histogram size to
	# choose (not fixed 512) and there would be multipication instead of sll.
	sll	$s6, $s6, 9
	divu	$s6, $s6, $s5	# divide by max element (normalize all elements to 512)
	beqz	$s6, draw_column_end
	multu	$s6, $t8	# find offset (number of columns * constant offest of one column difference)
	mflo	$t1	
	addu	$t1, $t0, $t1	# address of the top of the column
draw_column_loop:
	sb	$t9, ($t1)
	subu	$t1, $t1, $t8	# go to the next byte one column below
	bne	$t1, $t0, draw_column_loop
draw_column_end:
	jr	$ra		# return to the calling procedure
	
error_opening_histogram:
	PRINT_STR("Error opening histogram file to write:\n")
	li	$v0, 4
	la	$a0, filename
	syscall
	# MARS makes it impossible to free memory - look comment after initialization
	EXIT
		
error_saving_histogram:
	li	$v0, 16		# close file
	syscall
	PRINT_STR("Error saving histogram in file:\n")
	li	$v0, 4
	la	$a0, filename
	syscall
	# MARS makes it impossible to free memory - look comment after initialization
	EXIT
	
##################################################################################################################
# END of drawing histogram
##################################################################################################################

# Print nonzero histogram values (not used in release version)

	PRINT_STR("\nHistogram BLUE:\n")
	move	$t0, $s2
	addu	$t1, $t0, 1024
print_histogram_BLUE:
	lw	$t2, ($t0)
	bnez	$t2, nonzero_BLUE
	addu	$t0, $t0, 4
	bne	$t0, $t1, print_histogram_BLUE
	j	end_BLUE
nonzero_BLUE:
	li	$v0, 1
	subu	$a0, $t0, $s2
	srl	$a0, $a0, 2
	syscall
	PRINT_STR(" : ")
	li	$v0, 1
	move	$a0, $t2
	syscall
	PRINT_STR("\n")
	addu	$t0, $t0, 4
	bne	$t0, $t1, print_histogram_BLUE
end_BLUE:
	
					
	PRINT_STR("\nHistogram GREEN:\n")
	move	$t0, $s3
	addu	$t1, $t0, 1024
print_histogram_GREEN:
	lw	$t2, ($t0)
	bnez	$t2, nonzero_GREEN
	addu	$t0, $t0, 4
	bne	$t0, $t1, print_histogram_GREEN
	j 	end_GREEN
nonzero_GREEN:
	li	$v0, 1
	subu	$a0, $t0, $s3
	srl	$a0, $a0, 2
	syscall
	PRINT_STR(" : ")
	li	$v0, 1
	move	$a0, $t2
	syscall
	PRINT_STR("\n")
	addu	$t0, $t0, 4
	bne	$t0, $t1, print_histogram_GREEN
end_GREEN:	
	

	PRINT_STR("\nHistogram RED:\n")
	move	$t0, $s4
	addu	$t1, $t0, 1024
print_histogram_RED:
	lw	$t2, ($t0)
	bnez	$t2, nonzero_RED
	addu	$t0, $t0, 4
	bne	$t0, $t1, print_histogram_RED
	j	end_RED
nonzero_RED:
	li	$v0, 1
	subu	$a0, $t0, $s4
	srl	$a0, $a0, 2
	syscall
	PRINT_STR(" : ")
	li	$v0, 1
	move	$a0, $t2
	syscall
	PRINT_STR("\n")
	addu	$t0, $t0, 4
	bne	$t0, $t1, print_histogram_RED
end_RED:


EXIT
